using Ivannuari;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MateriPage : Page
{
    [SerializeField] private Button b_home;

    [SerializeField] private Button b_kiri;
    [SerializeField] private Button b_kanan;

    [SerializeField] private Image frameGambar;
    [SerializeField] private Sprite[] semuaGambar;

    [SerializeField] private TMP_Text label_nomor;
    private int nomor = 0;

    private void OnEnable()
    {
        nomor = 0;
        SetupGambar();
    }

    protected override void Start()
    {
        base.Start();

        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        b_kiri.onClick.AddListener(() =>
        {
            if(nomor <= 0)
            {
                return;
            }
            nomor--;
            SetupGambar();
        });

        b_kanan.onClick.AddListener(() =>
        {
            if(nomor == 15)
            {
                return;
            }
            nomor++;
            SetupGambar();
        });
    }

    private void SetupGambar()
    {
        frameGambar.sprite = semuaGambar[nomor];
        label_nomor.text = $"{nomor + 1} / 16";
    }
}
