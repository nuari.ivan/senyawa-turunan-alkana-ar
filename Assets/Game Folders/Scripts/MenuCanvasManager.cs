using Ivannuari;
using System.Collections;
using UnityEngine;

public class MenuCanvasManager : CanvasManager
{
    private void OnEnable()
    {
        if(GameManager.Instance == null)
        {
            StartCoroutine(FindGameManager());
            return;
        }
        GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
    }

    IEnumerator FindGameManager()
    {
        yield return new WaitForSeconds(1f);
        GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
    }

    private void Start()
    {
        AudioManager.Instance.HentikanSemuaSuara();
        AudioManager.Instance.MainkanSuara("BGM");
    }

    private void OnDisable()
    {
        GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
    }

    private void Instance_OnStateChanged(GameState newState)
    {
        switch (newState)
        {
            case GameState.Menu:
                ShowPage(PageName.Menu);
                break;
            case GameState.Profile:
                ShowPage(PageName.Profile);
                break;
            case GameState.Setting:
                ShowPage(PageName.Setting);
                break;
            case GameState.AR:
                break;
            case GameState.Petunjuk:
                ShowPage(PageName.Petunjuk);
                break;
            case GameState.Informasi:
                ShowPage(PageName.Informasi);
                break;
            case GameState.eModul:
                ShowPage(PageName.eModul);
                break;
            case GameState.Materi:
                ShowPage(PageName.Materi);
                break;
            case GameState.Evaluasi:
                ShowPage(PageName.Evaluasi);
                break;
            case GameState.Kompetensi:
                ShowPage(PageName.Kompetensi);
                break;
            case GameState.Exit:
                ShowWidget(WidgetName.Exit);
                break;
            case GameState.Result:
                ShowWidget(WidgetName.Result);
                break;
        }
    }
}
