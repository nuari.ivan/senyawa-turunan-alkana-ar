using Ivannuari;
using UnityEngine;
using UnityEngine.UI;

public class ExitWidget : Widget
{
    [SerializeField] private Button b_ya;
    [SerializeField] private Button b_tidak;

    private void Start()
    {
        b_ya.onClick.AddListener(() => Application.Quit());
        b_tidak.onClick.AddListener(() =>
        { 
            Tutup();
            GameManager.Instance.ChangeState(GameState.Menu);
        });
    }
}
