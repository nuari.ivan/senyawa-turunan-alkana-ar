using Ivannuari;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingPage : Page
{
    [SerializeField] private Button b_home;

    [SerializeField] private Slider[] allSliders;
    [SerializeField] private TMP_Text[] label_values;

    protected override void Start()
    {
        base.Start();
    
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));

        allSliders[0].onValueChanged.AddListener((float v) =>
        {
            label_values[0].text = $"{Mathf.FloorToInt(v * 100)} %";
            AudioManager.Instance.SetVolumeFx(v);
        });
        
        allSliders[1].onValueChanged.AddListener((float v) =>
        {
            label_values[1].text = $"{Mathf.FloorToInt(v * 100)} %";
            AudioManager.Instance.SetVolumeFx(v);
        });

        allSliders[0].value = 0.6f;
        allSliders[1].value = 0.4f;
    }
}
