using Ivannuari;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ArPage : Page
{
    [SerializeField] private Button b_home;
    [SerializeField] private Button b_information;
    [SerializeField] private Button b_restart;

    [SerializeField] private Button b_zoomIn;
    [SerializeField] private Button b_zoomOut;
    [SerializeField] private Button b_turnLeft;
    [SerializeField] private Button b_turnRight;

    [SerializeField] private Button[] b_items;

    [SerializeField] private TMP_Text label_nama;

    [SerializeField] private GameObject[] allObjects;
    [SerializeField] private string[] allTitles;

    [SerializeField] private string[] allInformasion;
    [SerializeField] private TMP_Text label_informasi;

    private int activeObject = 0;

    protected override void Start()
    {
        base.Start();
    
        b_home.onClick.AddListener(() => ChangeScene("Main Menu"));
        b_restart.onClick.AddListener(() =>
        {
            Scene s = SceneManager.GetActiveScene();
            SceneManager.LoadScene(s.name);
        });

        b_zoomIn.onClick.AddListener(() => Perbesar(true));
        b_zoomOut.onClick.AddListener(() => Perbesar(false));
        b_turnLeft.onClick.AddListener(() => PutarKanan(false));
        b_turnRight.onClick.AddListener(() => PutarKanan(true));

        b_items[0].onClick.AddListener(() =>
        {
            SetObject(0);
        });
        b_items[1].onClick.AddListener(() =>
        {
            SetObject(1);
        });
        b_items[2].onClick.AddListener(() =>
        {
            SetObject(2);
        });
        b_items[3].onClick.AddListener(() =>
        {
            SetObject(3);
        });
        b_items[4].onClick.AddListener(() =>
        {
            SetObject(4);
        });
        b_items[5].onClick.AddListener(() =>
        {
            SetObject(5);
        });
        b_items[6].onClick.AddListener(() =>
        {
            SetObject(6);
        });

        b_information.onClick.AddListener(ShowInformation);
    }

    private void ShowInformation()
    {
        if(label_informasi.gameObject.activeInHierarchy)
        {
            label_informasi.gameObject.SetActive(false);
        }
        else
        {
            label_informasi.gameObject.SetActive(true);
            label_informasi.text = $"{allInformasion[activeObject]}";
        }
    }

    public void PutarKanan(bool ya)
    {
        foreach (var obj in allObjects)
        {
            if(ya)
            {
                obj.transform.Rotate(0f, 2.5f, 0f);
            }
            else
            {
                obj.transform.Rotate(0f, -2.5f, 0f);
            }
        }
    }

    private void Perbesar(bool ya)
    {
        foreach (var obj in allObjects)
        {
            if(ya)
            {
                obj.transform.localScale *= 1.5f;
            }
            else
            {
                obj.transform.localScale /= 1.5f;
            }
        }
    }

    private void SetObject(int n)
    {
        activeObject = n;

        foreach (var item in allObjects)
        {
            item.SetActive(false);
        }
        allObjects[n].SetActive(true);
        label_nama.text = allTitles[n];
    }
}
