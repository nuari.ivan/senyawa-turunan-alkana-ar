using Ivannuari;
using UnityEngine;
using UnityEngine.UI;

public class MenuPage : Page
{
    [SerializeField] private Button b_informasi;
    [SerializeField] private Button b_setting;
    [SerializeField] private Button b_profil;

    [SerializeField] private Button b_exit;

    [SerializeField] private Button b_ar;
    [SerializeField] private Button b_kompetensi;
    [SerializeField] private Button b_eModul;
    [SerializeField] private Button b_materi;
    [SerializeField] private Button b_evaluasi;

    protected override void Start()
    {
        base.Start();
    
        b_informasi.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Informasi));
        b_setting.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Setting));
        b_profil.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Profile));

        b_kompetensi.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Kompetensi));
        b_eModul.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.eModul));
        b_materi.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Materi));
        b_evaluasi.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Evaluasi));

        b_ar.onClick.AddListener(() =>
        {
            GameManager.Instance.ChangeState(GameState.AR);
            ChangeScene("AR Scene");
        });
        b_exit.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Exit));
    }
}
