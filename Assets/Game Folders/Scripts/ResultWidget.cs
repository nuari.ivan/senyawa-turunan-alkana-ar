using Ivannuari;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResultWidget : Widget
{
    [SerializeField] private Button b_home;
    [SerializeField] private Button b_menu;
    [SerializeField] private TMP_Text label_result;
    [SerializeField] private TMP_Text label_timer;

    private void OnEnable()
    {
        b_home.onClick.AddListener(() => 
        { 
            GameManager.Instance.ChangeState(GameState.Menu);
            this.gameObject.SetActive(false);
        });

        b_menu.onClick.AddListener(() =>
        {
            GameManager.Instance.ChangeState(GameState.Evaluasi);
            this.gameObject.SetActive(false);
        });

        label_result.text = $"{PlayerPrefs.GetInt(GameManager.SCORE)}";
        CalculateTimer();

    }

    private void CalculateTimer()
    {
        int t = PlayerPrefs.GetInt(GameManager.TIMER);
        if (t >= 60)
        {
            label_timer.text = $"01 : {t - 60}";
        }
        else
        {
            if(t >= 10)
            {
                label_timer.text = $"00 : {t}";
            }
            else
            {
                label_timer.text = $"00 : 0{t}";
            }
            
        }
    }
}
