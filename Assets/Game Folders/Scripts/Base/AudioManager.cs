using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    [SerializeField] private Sound[] allSounds;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (var s in allSounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.klip;
            s.source.volume = s.vol;
            s.source.loop = s.berulang;
        }
    }

    public void MainkanSuara(string namaSuara)
    {
        Sound findSound = Array.Find(allSounds, s => s.nama == namaSuara);
        if(findSound != null)
        {
            findSound.source.Play();
        }
    }

    public void HentikanSemuaSuara()
    {
        foreach (var s in GetComponents<AudioSource>())
        {
            s.Stop();
        }
    }

    public void SetVolumeFx(float value)
    {
        Sound findSound = Array.Find(allSounds, s => s.nama == "klik");
        if (findSound != null)
        {
            findSound.source.volume = value;
        }
    }

    public void SetVolumeBgm(float value)
    {
        Sound findSound = Array.Find(allSounds, s => s.nama == "BGM");
        if (findSound != null)
        {
            findSound.source.volume = value;
        }
    }
}
