using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Ivannuari 
{
    public class Page : MonoBehaviour
    {
        public PageName nama;

        protected virtual void Start()
        {
            Button[] allButtons = GetComponentsInChildren<Button>();
            foreach (var b in allButtons)
            {
                b.onClick.AddListener(() => AudioManager.Instance.MainkanSuara("klik"));
            }
        }

        protected void ChangeScene(string namaScene)
        {
            SceneManager.LoadScene(namaScene);
        }
    }
}
