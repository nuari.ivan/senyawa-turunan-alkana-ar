using UnityEngine;

namespace Ivannuari
{
    public class Widget : MonoBehaviour
    {
        public WidgetName nama;
        protected void Tutup()
        {
            this.gameObject.SetActive(false);
        }
    }
}
