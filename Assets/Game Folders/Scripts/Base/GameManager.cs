using UnityEngine;

namespace Ivannuari
{

    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        [SerializeField] private GameState currentState = GameState.Menu;

        public delegate void GameStateDelegate(GameState newState);
        public event GameStateDelegate OnStateChanged;

        public static string SCORE = "score";
        public static string TIMER = "timer";

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        public void ChangeState(GameState newState)
        {
            if(newState == currentState)
            {
                return;
            }

            currentState = newState;

            OnStateChanged?.Invoke(currentState);
        }
    }
}
