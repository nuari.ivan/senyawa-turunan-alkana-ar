namespace Ivannuari
{
    public enum GameState
    {
        Menu,
        Profile,
        Setting,
        AR,
        Petunjuk,
        Informasi,
        eModul,
        Materi,
        Evaluasi,
        Kompetensi,
        Exit,
        Result
    }

    public enum PageName
    {
        Menu,
        Profile,
        Setting,
        AR,
        Petunjuk,
        Informasi,
        eModul,
        Materi,
        Evaluasi,
        Kompetensi
    }

    public enum WidgetName
    {
        Exit,
        Result
    }
}