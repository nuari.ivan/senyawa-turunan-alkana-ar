using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public string nama;
    public AudioClip klip;
    public float vol;
    public bool berulang;

    [HideInInspector]
    public AudioSource source;
}
