using System;
using UnityEngine;

namespace Ivannuari
{
    public class CanvasManager : MonoBehaviour
    {
        [SerializeField] protected Page[] allPages;
        [SerializeField] protected Widget[] allWidgets;

        protected void ShowPage(PageName newPage)
        {
            foreach (var p in allPages)
            {
                if (p != null)
                {
                    p.gameObject.SetActive(false);
                }
            }

            Page findPage = Array.Find(allPages, p => p.nama == newPage);
            if(findPage != null)
            {
                findPage.gameObject.SetActive(true);
            }
        }

        protected void ShowWidget(WidgetName newWidget)
        {
            foreach (var w in allWidgets)
            {
                if (w != null)
                {
                    w.gameObject.SetActive(false);
                }
            }

            Widget findWidget = Array.Find(allWidgets, w => w.nama == newWidget);
            if (findWidget != null)
            {
                findWidget.gameObject.SetActive(true);
            }
        }
    }
}
