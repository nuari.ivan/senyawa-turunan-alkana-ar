using Ivannuari;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EvaluasiPage : Page
{
    [SerializeField] private Button b_home;
    [SerializeField] private Button b_menu;
    [SerializeField] private DaftarSoal[] paketSoal;

    [SerializeField] private GameObject[] allPanels;
 
    [Header("MENU")]
    [SerializeField] private Button b_struktur;
    [SerializeField] private Button b_tataNama;
    [SerializeField] private Button b_isomer;
    [SerializeField] private Button b_sifatDanSintesis;

    [Header("SOAL OBJECT")]
    [SerializeField] private TMP_Text label_namaPaket;
    [SerializeField] private TMP_Text label_namaScore;
    [SerializeField] private TMP_Text label_nomor;
    [SerializeField] private TMP_Text label_timer;

    [SerializeField] private GameObject panelResult;
    [SerializeField] private TMP_Text label_result;

    [SerializeField] private TMP_Text label_pertanyaan;
    [SerializeField] private TMP_Text[] label_opsi;

    private int nomorPaket , nomorSoal , score , timer;

    protected override void Start()
    {
        base.Start();
    
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));

        b_menu.onClick.AddListener(() =>
        {
            allPanels[0].SetActive(true);
            allPanels[1].SetActive(false);
        });

        b_struktur.onClick.AddListener(() => OpenQuiz(Paket.Struktur));
        b_tataNama.onClick.AddListener(() => OpenQuiz(Paket.TataNama));
        b_isomer.onClick.AddListener(() => OpenQuiz(Paket.Isomer));
        b_sifatDanSintesis.onClick.AddListener(() => OpenQuiz(Paket.SifatDanSintesis));
    }

    private void OpenQuiz(Paket newPaket)
    {
        allPanels[0].SetActive(false);
        allPanels[1].SetActive(true);
        nomorSoal = 0;
        timer = 0;
        nomorPaket = (int)newPaket;

        StartSoal(nomorPaket, nomorSoal);
    }

    private void StartSoal(int p, int s)
    {
        StartCoroutine(HitungWaktu());
        label_namaPaket.text = paketSoal[p].namaPaket;
        label_pertanyaan.text = paketSoal[p].semuaSoal[s].pertanyaan;
        label_nomor.text = $"{nomorSoal + 1} / 5";
        for (int i = 0; i < 4; i++)
        {
            label_opsi[i].text = paketSoal[p].semuaSoal[s].opsi[i];
        }
    }

    IEnumerator HitungWaktu()
    {
        int t = 15;
        for (int i = 0; i < 15; i++)
        {
            t -= 1;
            label_timer.text = $"00 : {t}";
            yield return new WaitForSeconds(1f);
            timer++;
        }

        JawabanBenar(false);
    }

    public void PilihanJawaban(int jawaban)
    {
        if(jawaban == paketSoal[nomorPaket].semuaSoal[nomorSoal].jawaban)
        {
            //benar
            JawabanBenar(true);
        }
        else
        {
            //salah
            JawabanBenar(false);
        }
    }

    private void JawabanBenar(bool ya)
    {
        StopAllCoroutines();
        if(ya)
        {
            score += 5;
            label_namaScore.text = $"{score}";
            panelResult.SetActive(true);
            label_result.text = $"Kamu Benar ! \n  <color=blue><size=35>poin +5</color></size>";
            AudioManager.Instance.MainkanSuara("benar");
        }
        else
        {
            label_namaScore.text = $"{score}";
            panelResult.SetActive(true);
            label_result.text = $"Kamu Salah ! \n <color=red><size=35>Selanjutnya jangan salah lagi ya</color></size>";
            AudioManager.Instance.MainkanSuara("salah");
        }

        StartCoroutine(HidePanelResult());
    }

    IEnumerator HidePanelResult()
    {
        yield return new WaitForSeconds(1.5f);
        panelResult.SetActive(false);

        nomorSoal++;

        if(IsFinish())
        {
            PlayerPrefs.SetInt(GameManager.SCORE, score);
            PlayerPrefs.SetInt(GameManager.TIMER, timer);
            yield return new WaitForSeconds(0.5f);
            GameManager.Instance.ChangeState(GameState.Result);

            allPanels[0].SetActive(true);
            allPanels[1].SetActive(false);
        }
        else
        {
            if (nomorSoal >= paketSoal[nomorPaket].semuaSoal.Length)
            {
                nomorPaket++;
                nomorSoal = 0;
            }

            StartSoal(nomorPaket, nomorSoal);
        }
    }

    private bool IsFinish()
    {
        return nomorSoal >= 5;
    }
}

[System.Serializable]
public class Soal
{
    [TextArea(3,3)] public string pertanyaan;
    [TextArea(2,1)] public string[] opsi;
    public int jawaban;
}

[System.Serializable]
public class DaftarSoal
{
    public string namaPaket;
    public Soal[] semuaSoal;
}

public enum Paket
{
    Struktur,
    TataNama,
    Isomer,
    SifatDanSintesis
}
