using Ivannuari;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EModulPage : Page
{
    [SerializeField] private Button b_home;
    [SerializeField] private TMP_InputField input_link;
    [SerializeField] private Button b_openUrl;

    [SerializeField] private string uri;

    private UniWebView webView;

    protected override void Start()
    {
        webView = GetComponent<UniWebView>();
        base.Start();
    
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        input_link.text = uri;
        b_openUrl.onClick.AddListener(() => Application.OpenURL(uri));
        webView.Show();
    }
}
